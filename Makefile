PROGRAM=password

TEST_DIR=assemblyTests
TEST_CASES=$(TEST_DIR)/testcases
TEST_RESULTS=$(TEST_DIR)/results

SOURCE_DIR=product
BUILD_DIR=$(SOURCE_DIR)/bin

CPPFLAGS=-std=gnu++11

AFLCPP=afl-g++
AFLFLAGS=$(CPPFLAGS) -fno-stack-protector -z execstack

$(PROGRAM):
	mkdir -p $(BUILD_DIR)
	g++ $(CPPFLAGS) $(SOURCE_DIR)/main.c -o $(BUILD_DIR)/$@

$(PROGRAM)-fuzz:
	mkdir -p $(BUILD_DIR)
	$(AFLCPP) $(AFLFLAGS) $(SOURCE_DIR)/main.c -o $(BUILD_DIR)/$@

.PHONY: clean fuzz
clean:
	rm -rf product/bin/ $(TEST_RESULTS)/

test: $(PROGRAM)-fuzz
	afl-fuzz -i $(TEST_CASES)/ -o $(TEST_RESULTS)/ $(BUILD_DIR)/$(PROGRAM)-fuzz

blackboxtest: $(PROGRAM)
	afl-fuzz -Q -i $(TEST_CASES)/ -o $(TEST_RESULTS)/ $(BUILD_DIR)/$(PROGRAM)