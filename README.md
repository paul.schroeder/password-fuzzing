# Minimales American Fuzzy Lop (afl) Beispiel

Fuzzy Testing oder Fuzzing ist eine Methode des Softwaretesting. Dabei geht es darum, dass der Input von Programmen oder Systemes zufälling generiert/variiert wird und damit fehlfunkionen im Programm provoziert werden sollen um unvorhergesehenes Verhalten auszulösen und unerkannte Bugs in Kraft treten zu lassen. Ein Programm, dass dies Vorgehen (Input erzeugung, Programmstart mit entsprechenden Input und Logging des Outcomes) automatisiert ist [American fuzzy lop][afl] (AFL).

Hier soll anhand eines MInimalbeispiels das generelle Vorgehen zum Fuzzing mit AFL erklärt werden.

# With or Without Source
Mit AFL gibt es zwei Möglichkeiten ein Programm zu Fuzzen. Wenn der Source verfügbar ist, wird diese mit einem `gcc`-wrapper von AFL dem `afl-gcc` compiliert und resultiert dann in einer Binary mit der AFL einen Blick in den Programmablauf zu werfen kann. Hierdurch können Pfade gezielt angesteuert werden um so die Abdeckung zu erhöhen.

Steht der Source nicht zu verfügung, wird das Programm als Blackbox behandelt. Um einen ähnlich große Kontrolle über den Programmfluß zu erhalten bedient sich AFL eines gepatchten `qemu`s was ebenfalls im AFL repo enthalten ist. Wegen des Emulationsoverheads ist diese Variante jedoch 2-5x langsamer als erstere Mathode.

### Installation
Zunächst sollten (für Ubuntu) folgende Dependecies, sofern nicht schon vorhanden, installiert werden:
```bash
$ sudo apt-get update
$ sudo apt-get install build-essential
```
Anschliesend clone, build, install mit:
```bash
$ git clone https://github.com/mirrorer/afl
$ cd extern/afl/
$ make
$ sudo make install
```
Ist Blackbox testing nötig, muss vor dem `make install` Folgendes ausgeführt werden:
```bash
$ cd qemu_mode/
$ sudo apt-get install libtool-bin automake bison libglib2.0-dev
$ ./build_qemu_support.sh
$ cd ../
```
# Beispiel
Wenn der Source vorhanden ist, ist das beste Vorgehen diesen mittels `CC=/path/to/afl/afl-gcc` oder `CXX=/path/to/afl/afl-g++` zu compilieren. Als Beispiel betrachten wir folgendes Programm `product/main.c`, eine Passwortabfrage:
```c
#include <stdio.h>
#include <string.h>

int main(void) {
    char user[16];	// kept small to provoke early crashes
    char pass[16];
    
    const char secret_user[] = "root";
    const char secret_pass[] = "1234";
    
    printf("Login: ");
    gets(user);
    
    printf("Password: ");
    gets(pass);
    	
    if( 0 == strcmp(user, secret_user) ) {
        // path 2: valid user, invalid pass
		
        if ( 0 == strcmp(pass, secret_pass) ) {
            // path 3: valid user & pass
			
            printf("Access granted.\n");
            return 0;
        }
    }
	
    // path 1: invalid user

    printf("Access denied.\n");
    return 1;
}
```
Zunächst sei Bemerkt, dass `gets()` verwendet wird, was kein gutes vorgehen ist. Diese Funktion schreibt eingaben vom `STDIN` in den angegebene Puffer (`user` bzw. `pass`), jedoch ohne die Größe des Puffers zu kennen. Sie schreibt also kontinuierlich in den Speicher bis sie eine neue Zeile oder ein `EOF` findet. Dies kann bei genügend großem Input zu einer Bufferoverflow führen. Im weiteren Ablauf ergebe sich drei Pfade des Programms:
1. Falscher User
2. Richtiger User aber falsches Passwort
3. Richtiger User und richtiges Passwort

Diese drei Fälle werden sich später auch in den Testcases wiederfinden.
# Compilieren
Damit dieses simple Beispiel interessant wird, müssen ein paar Sicherheitsmechanismen, die der `gcc` von Haus aus mitbringt, ausgeschaltet werden. Die Compile Commandline lautet:
```bash
$ afl-gcc -fno-stack-protector -z execstack product/main.c -o product/bin/password
```
Die unstripped `afl-gcc` binary hat eine gewisses plus an Größe. Auf dem Testsystem waren das ca. +105%. 
# Testcases
Vor Begin des fuzzens muss AFL über ein gutes Beispiel, des regulär zu erwartenden Inputs verfügen. Nach AFL Dokumentation sollten ein Testcase idealerweise <1kB sein und mehrere sollten nur dann verwendet werden, wenn sie sich Funktional unterscheiden. In unserem Fall ergeben sich, wie schon gesagt, drei Testcases, die in seperate Dateien in einem Ordner (`assemblyTests/testcases`) gesammelt abgelegt werden:

`test1`:
```
wrong_user
wrong_pass
```
`test2`:
```
root
wrong_pass
```
`test3`:
```
root
1234
```
Testcase replay erfolgt dann einfach per `cat assemblyTests/testcases/test2 | product/bin/password`

# Fuzzing
AFL kann nun via
```
afl-fuzz -i assemblyTests/testcases/ -o assemblyTests/results/ product/bin/password
```
gestartet werden (Blackbox fuzzing wird mit `-Q` switch ausgeführt). Ergebnisse werden nun im Subfolder `assemblyTests/results/` gespeichert. Inputs die zu Crashes geführt haben, liegen für weitere Untersuchung in `assemblyTests/results/crashes/` und können wie oben beschrieben gereplayed werden.
> **Beachte:**
> Die Namen der Craschreports in unter `assemblyTests/results/crashes/` können sich von system zu system unterscheiden.

# AFL
Ist AFL gestartet wird sollte folgendes erscheinen:

![alt text](https://gitlab.com/paul.schroeder/password-fuzzing/raw/master/doc/afl.png "AFL Screenshot")

Interessant sind hier vor allem `exec speed` unter `stage progress`, `cycles done` und `total paths` unter `overall results` sowie `finding in depth`
# Parallelisierung
Das Fuzzen kann auch parallelisiert werden, indem man ein Masterprozessvon und mehere Workerprozesse von AFL startet:
```bash
$ ./afl-fuzz -i testcase -o sync_dir -M fuzzerMaster [...other stuff...]
$ ./afl-fuzz -i testcase -o sync_dir -S fuzzerWorker1 [...other stuff...]
$ ./afl-fuzz -i testcase -o sync_dir -S fuzzerworker2 [...other stuff...]
```
Die Ergebnisse aller Wroker und des Masters werden dann im `sync_dir` gespeichert.

# Dauer des Fuzzens
Nach einer gewissen Zeit kann das Fuzzen gestoppt werden. Die  Dauer hängt stark von der Komplexität des Programms ab, aber üblicherweise brauchen interessantere Ergebnisse länger..

[afl]: <https://github.com/mirrorer/afl/>