#include <stdio.h>
#include <string.h>

int main(void) {
	char user[16];	// kept samll to provoke early crashes
	char pass[16];
	
	const char secret_user[] = "root";
	const char secret_pass[] = "1234";
	
	printf("Login: ");
	gets(user);
	
	printf("Password: ");
	gets(pass);
	
	// path 1: invalid user
	if( 0 == strcmp(user, secret_user) ) {
		// path 2: valid user, invalid pass
		if ( 0 == strcmp(pass, secret_pass) ) {
			// path3: valid user & pass
			printf("Access granted.\n");
			return 0;
		}
	}

	printf("Access denied.\n");
	return 1;
}